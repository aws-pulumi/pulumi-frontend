import * as aws from '@pulumi/aws'

import envVars from './envVars'

const originAccessIdentity = new aws.cloudfront.OriginAccessIdentity('pulumiOAI', {
  comment: `Used by pulumi for project ${envVars.AWS_PREFIX}`,
})

export default originAccessIdentity
