import * as aws from '@pulumi/aws'

import envVars from '../envVars'

import botControlRuleSet from './rules/botControlRuleSet.json'
import amazonIpReputationList from './rules/amazonIpReputationList.json'
import anonymousIpList from './rules/anonymousIpList.json'
import commonRuleSet from './rules/commonRuleSet.json'
import knownBadInputsRuleSet from './rules/knownBadInputsRuleSet.json'
import linuxRuleSet from './rules/linuxRuleSet.json'
import sQLiRuleSet from './rules/sQLiRuleSet.json'


const wafWebAclName = `${envVars.AWS_PREFIX}-WAF`

const wafWebAcl = new aws.wafv2.WebAcl(wafWebAclName, {
  defaultAction: {
    allow: {},
  },

  description: `Firewall rules to protect the CloudFront instance of project ${envVars.AWS_PREFIX}`,

  scope: 'CLOUDFRONT',

  tags: {
    Name: wafWebAclName,
  },

  visibilityConfig: {
    metricName: wafWebAclName,
    sampledRequestsEnabled: false,
    cloudwatchMetricsEnabled: false,
  },

  rules: [
    botControlRuleSet,
    amazonIpReputationList,
    anonymousIpList,
    commonRuleSet,
    knownBadInputsRuleSet,
    linuxRuleSet,
    sQLiRuleSet,
  ],
})


export default wafWebAcl
