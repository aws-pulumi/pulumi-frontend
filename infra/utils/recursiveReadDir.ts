import fs from 'fs'
import path from 'path'

/**
 * Recursively loops through all files and directories in `dirPath`
 * and returns an array of all items paths.
 *
 * @param {string} dirPath What is the relative directory path
 * @return {string[]} Array of files and directories of files within the `dirPath`
 */
const recursiveReadDir = (dirPath: string): string[] => {
  return fs.readdirSync(dirPath, { withFileTypes: true })
    .map((itemPath) => {
      if (itemPath.isDirectory()) {
        return recursiveReadDir(path.join(dirPath, itemPath.name))
          .map((resolvedPath) => `${itemPath.name}/${resolvedPath}`)
      }
      return itemPath.name
    })
    .flat()
}


export default recursiveReadDir
