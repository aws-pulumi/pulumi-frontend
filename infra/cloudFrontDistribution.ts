import * as aws from '@pulumi/aws'

import envVars from './envVars'
import bucket, { bucketName } from './bucket'
import originAccessIdentity from './originAccessIdentity'
import wafWebAcl from './wafWebAcl'

/**
 * If we need to deploy a CloudFront distribution
 * with alternative domain, e.g. "dev.my-app.com"
 * then we use the optional ENV VARs `ALIAS_DOMAIN_NAME` and `DOMAIN_NAME_ACM_CERTIFICATE_ARN`
 *
 * Please know that this `ALIAS_DOMAIN_NAME` need to be registered as Route 53 Records as well.
 */
const aliases = envVars.ALIAS_DOMAIN_NAME && envVars.DOMAIN_NAME_ACM_CERTIFICATE_ARN
  ? [envVars.ALIAS_DOMAIN_NAME]
  : undefined

const viewerCertificate = envVars.ALIAS_DOMAIN_NAME && envVars.DOMAIN_NAME_ACM_CERTIFICATE_ARN
  ? {
    acmCertificateArn: envVars.DOMAIN_NAME_ACM_CERTIFICATE_ARN,
    sslSupportMethod: 'sni-only',
  } : {
    cloudfrontDefaultCertificate: true,
  }


const cloudFrontDistribution = new aws.cloudfront.Distribution(
  `${envVars.AWS_PREFIX}-cloudFrontDistribution-for-${bucketName}`,
  {
    enabled: true,

    origins: [{
      originId: bucketName,
      domainName: bucket.bucketRegionalDomainName,
      s3OriginConfig: {
        originAccessIdentity: originAccessIdentity.cloudfrontAccessIdentityPath,
      },
    }],

    webAclId: wafWebAcl.arn,

    aliases,
    viewerCertificate,

    /* Single Page Applications always start from the `index.html` */
    defaultRootObject: 'index.html',

    /**
     * SPA routing of `http://my-app.com/dashboard`
     * is handled by the root `index.html` file
     */
    customErrorResponses: [{
      errorCode: 403,
      responsePagePath: '/index.html',
      responseCode: 200,
    }],

    defaultCacheBehavior: {
      allowedMethods: [
        'HEAD',
        'GET',
        'OPTIONS',
        'PATCH',
        'POST',
        'PUT',
        'DELETE',
      ],
      cachedMethods: [
        'HEAD',
        'GET',
      ],
      targetOriginId: bucketName,
      forwardedValues: {
        queryString: false,
        cookies: {
          forward: 'none',
        },
      },
      viewerProtocolPolicy: 'redirect-to-https',
      defaultTtl: envVars.CLOUD_FRONT_DEFAULT_TTL,
      maxTtl: envVars.CLOUD_FRONT_MAX_TTL,
    },

    /**
     * For distribution regions
     * use only North America and Europe
     */
    priceClass: 'PriceClass_100',

    restrictions: {
      geoRestriction: {
        restrictionType: 'none',
        locations: [],
      },
    },
  },
)


export default cloudFrontDistribution
