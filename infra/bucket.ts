import * as mime from 'mime'
import * as path from 'path'

import * as aws from '@pulumi/aws'
import * as pulumi from '@pulumi/pulumi'

import envVars from './envVars'
import originAccessIdentity from './originAccessIdentity'
import recursiveReadDir from './utils/recursiveReadDir'

export const bucketName = `${envVars.AWS_PREFIX}-FrontEndBucket`


const bucket = new aws.s3.Bucket(bucketName, {
  acl: 'private',
})

/**
 * We make a `private` bucket and allow read access only to user `originAccessIdentity`
 * just to make sure that files in the bucket can be accessed only from the CloudFront CDN
 * and not from the S3 Bucket URL
 */
new aws.s3.BucketPolicy(`${bucketName}-bucketPolicy`, {
  bucket: bucket.id,
  policy: bucket.arn.apply((bucketArn) => originAccessIdentity.iamArn.apply((oaiIamArn) => JSON.stringify({
    Version: '2012-10-17',
    Id: 'PolicyForCloudFrontPrivateContent',
    Statement: [{
      Effect: 'Allow',
      Principal: {
        AWS: oaiIamArn,
      },
      Action: ['s3:GetObject'],
      Resource: [`${bucketArn}/*`],
    }],
  }))),
})

/**
 * Fill the secured bucket with all already created files
 * from Webpack if the `build` directory.
 */
const siteDir = path.relative(__dirname, '../build')
recursiveReadDir(siteDir)
  .forEach((itemPath) => {
    const filePath = path.join(siteDir, itemPath)
    new aws.s3.BucketObject(itemPath, {
      bucket,
      acl: 'private',
      source: new pulumi.asset.FileAsset(filePath),
      contentType: mime.getType(filePath) || undefined,
    })
  })


export default bucket
