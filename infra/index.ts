import * as pulumi from '@pulumi/pulumi'

import envVars from './envVars'
import { bucketName } from './bucket'
import cloudFrontDistribution from './cloudFrontDistribution'
import './route53Record'

export const bucketNameExport = bucketName
export const cloudFrontDistributionUrl = pulumi.interpolate`https://${cloudFrontDistribution.domainName}`
export const websiteUrl = envVars.ALIAS_DOMAIN_NAME ? `https://${envVars.ALIAS_DOMAIN_NAME}` : 'No ALIAS_DOMAIN_NAME set in EVN VAR'
