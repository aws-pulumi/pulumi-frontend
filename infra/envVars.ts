import dotenv from 'dotenv'
import Joi from 'joi'
import * as pulumi from '@pulumi/pulumi'

dotenv.config({ path: '../.env' })

const config = new pulumi.Config()

const schema = {
  AWS_PREFIX: Joi
    .string()
    .required(),

  CLOUD_FRONT_DEFAULT_TTL: Joi
    .number()
    .integer()
    .min(0)
    .max(365 * 24 * 60 * 60)
    .required(),

  CLOUD_FRONT_MAX_TTL: Joi
    .number()
    .integer()
    .min(0)
    .max(365 * 24 * 60 * 60)
    .required(),

  ALIAS_DOMAIN_NAME: Joi
    .string()
    .min(5)
    .max(500)
    .optional(),

  ROUTE_53_HOSTED_ZONE_ID: Joi
    .string()
    .min(2)
    .max(200)
    .optional(),

  DOMAIN_NAME_ACM_CERTIFICATE_ARN: Joi
    .string()
    .min(2)
    .max(200)
    .optional(),
}


/* Validate the expected ENV VARs */
const { value, error } = Joi.object().keys(schema)
  .required()
  .validate(
    {
      ...config.requireObject('envVars'),
      ...process.env,
    },
    {
      abortEarly: false,
      allowUnknown: true,
    },
  )

if (error) {
  process.stderr.write(`Invalid ENV VAR:
${error.details.map(({ message, context }) => `  ${message}; currently ${context?.key}=${context?.value}`).join('\n')}
\n`)
  process.exit(1)
}

/**
 * Be sure that the Server will run only with filtered, valid, and converted Env VARs
 * that no one can add / remove / edit
 */
export default Object.freeze(value)
