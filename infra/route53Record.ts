import * as aws from '@pulumi/aws'

import envVars from './envVars'
import cloudFrontDistribution from './cloudFrontDistribution'

/**
 * Should our app needs to be sered from a custom domain like `dev.my-app.com`
 * then we need to make sure we have the domain name in our AWS hosted zone.
 */
if (envVars.ALIAS_DOMAIN_NAME &&
    envVars.ROUTE_53_HOSTED_ZONE_ID
) {
  /**
   * Create an "alias" record between the custom domain `ALIAS_DOMAIN_NAME`
   * and our `cloudFrontDistribution` so we can serve traffic between them.
   */
  new aws.route53.Record(`Route53 alias record between ${envVars.ALIAS_DOMAIN_NAME} => CouldFront Distribution`, {
    zoneId: envVars.ROUTE_53_HOSTED_ZONE_ID,
    name: envVars.ALIAS_DOMAIN_NAME,
    type: 'A',
    aliases: [{
      name: cloudFrontDistribution.domainName,
      zoneId: cloudFrontDistribution.hostedZoneId,
      evaluateTargetHealth: true,
    }],
  })
}
