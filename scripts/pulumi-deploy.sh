#!/bin/bash

# Exit if a command returns a non-zero exit code and also
# print the commands and their args as they are executed
set -e -x

cd ./infra

curl -fsSL https://get.pulumi.com/ | bash
export PATH=$PATH:$HOME/.pulumi/bin
pulumi login

pulumi plugin install resource aws v4.12.0

pulumi stack select $PULUMI_STACK
pulumi up --yes
