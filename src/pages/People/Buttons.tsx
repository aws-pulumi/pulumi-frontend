import { useMemo } from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import memoize from 'lodash.memoize'

import { Button } from '@src/components'
import getRandomNumber from '@src/utils/getRandomNumber'


type ButtonsProps = {
  className?: string
  isLoading: boolean
}

const Buttons = ({
  className,
  isLoading,
}: ButtonsProps) => {
  const history = useHistory()
  const gotoPage = useMemo(
    () => memoize((id: number) => () => history.push(`/people/${id}`)),
    [history])

  return (
    <div className={className}>
      <div className="mx-auto">
        <div className="flex justify-center items-center space-x-10">
          <Button
            variant='primary'
            onClick={gotoPage(1)}
            isLoading={isLoading}
          >
            Luke
          </Button>

          <Button
            variant='danger'
            onClick={gotoPage(5)}
            isLoading={isLoading}
          >
            Leah
          </Button>

          <Button
            variant='default'
            onClick={gotoPage(getRandomNumber(1, 80))}
            isLoading={isLoading}
          >
            Random
          </Button>

          <Button
            variant='error'
            onClick={gotoPage(100000)}
            isLoading={isLoading}
          >
            Error
          </Button>
        </div>
      </div>
    </div>
  )
}


Buttons.propTypes = {
  className: PropTypes.string,
  isLoading: PropTypes.bool,
}

Buttons.defaultProps = {
  className: undefined,
  isLoading: false,
}

Buttons.displayName = 'Buttons'


export default Buttons
