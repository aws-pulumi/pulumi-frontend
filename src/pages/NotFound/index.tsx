import { AiFillWarning } from 'react-icons/ai'

const NotFound = () => (
  <div className="text-center text-2xl">
    <AiFillWarning className="text-5xl m-auto mb-6" />
    <div>
      Page Not Found
    </div>
  </div>
)

export default NotFound
