import { GetPersonResponse } from '@src/api/people/types'

import createStore from './createStore'


type PersonDataState = {
  person?: GetPersonResponse
}

type PersonState = PersonDataState & {
  setPerson: (person: GetPersonResponse | undefined) => void
  resetState: () => void
}


const initialState: PersonDataState = {
  person: undefined,
}

const usePerson = createStore<PersonState>((set) => ({
  ...initialState,

  setPerson: (person) => set((state) => {
    state.person = person
  }),

  resetState: () => set(() => initialState),
}))


export const selectPerson = (state: PersonState) => state.person
export const selectSetPerson = (state: PersonState) => state.setPerson
export const selectResetState = (state: PersonState) => state.resetState


export default usePerson
