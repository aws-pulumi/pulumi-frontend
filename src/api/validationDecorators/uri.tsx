import * as jf from 'joiful'

const uri = jf.string().uri().required()

export default uri
