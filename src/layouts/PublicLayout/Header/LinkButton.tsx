import PropTypes from 'prop-types'
import useReactRouter from 'use-react-router'
import { Link } from 'react-router-dom'

type LinkButtonProps = {
  path: string
  label: string
}

const LinkButton = ({
  path,
  label,
}: LinkButtonProps) => {
  const { location } = useReactRouter()

  return (
    <Link
      className={location.pathname.startsWith(path) ? 'text-black whitespace-nowrap' : 'hover:text-black whitespace-nowrap'}
      to={path}
    >
      {label}
    </Link>
  )
}


LinkButton.propTypes = {
  path: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
}

LinkButton.displayName = 'LinkButton'


export default LinkButton
