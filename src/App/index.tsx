import { QueryClientProvider } from 'react-query'
import { ToastContainer } from 'react-toastify'

import queryClient from '@src/api/queryClient'
import AppRouter from './AppRouter'

import 'react-toastify/dist/ReactToastify.css'
import './index.css'


const App = () => (
  <QueryClientProvider client={queryClient}>
    <ToastContainer
      position="top-right"
      autoClose={5_000}
      hideProgressBar
      pauseOnHover
    />

    <AppRouter />
  </QueryClientProvider>
)

export default App
