import { lazy } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Switch, Redirect } from 'react-router'

import LoadableRoute from './LoadableRoute'

import { PublicLayout } from '@src/layouts'

const Home = lazy(() => import(/* webpackChunkName: "Home" */ '@src/pages/Home'))
const People = lazy(() => import(/* webpackChunkName: "People" */ '@src/pages/People'))
const NotFound = lazy(() => import(/* webpackChunkName: "NotFound" */ '@src/pages/NotFound'))

const Routes = () => (
  <Router basename="/app">
    <Switch>
      <Redirect exact path="/" to="/home" />

      <LoadableRoute
        path="/home"
        component={Home}
        layout={PublicLayout}
      />

      <Redirect exact path="/people" to="/people/1" />
      <LoadableRoute
        path="/people/:urlPersonId"
        component={People}
        layout={PublicLayout}
      />

      <LoadableRoute
        component={NotFound}
        layout={PublicLayout}
      />
    </Switch>
  </Router>
)

export default Routes
