import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { VscLoading } from 'react-icons/vsc'

type DelayedFallbackProps = {
  minDelay: number
}

/**
 * Delays the Loading message so we do not only flash before quickly rendering the content.
 *
 * Credit goes to
 *    https://stackoverflow.com/a/58971161
 *
 * @param {object} props List of Component props:
 *   {number} [optional] `minDelay` defaults to 300 milliseconds - How much time to wait before showing a Loading message
 * @return {object} Loading React component
 */
const DelayedFallback = ({
  minDelay,
}: DelayedFallbackProps) => {
  const [showLoading, setShowLoading] = useState(false)

  useEffect(() => {
    const timeout = setTimeout(() => setShowLoading(true), minDelay)
    return () => clearTimeout(timeout)
  }, [minDelay])

  return showLoading ? (
    <div className="flex items-center justify-center text-3xl h-full">
      <VscLoading className="animate-spin" />
    </div>
  ) : null
}


DelayedFallback.propTypes = {
  minDelay: PropTypes.number,
}

DelayedFallback.defaultProps = {
  minDelay: 300,
}

DelayedFallback.displayName = 'DelayedFallback'


export default DelayedFallback
