import { ReactNode } from 'react'
import PropTypes from 'prop-types'


type ButtonProps = {
  variant: 'default' | 'primary' | 'danger' | 'error'
  onClick: () => void,
  isLoading: boolean
  children?: ReactNode
}

const getClassByVariant = (variant: ButtonProps['variant']) => {
  const className = 'flex justify-center items-center cursor-pointer border rounded w-24 h-12'

  if (variant === 'primary') {
    return `${className} border-blue-400 text-blue-600 hover:bg-blue-400 hover:text-white`
  }

  if (variant === 'danger') {
    return `${className} border-red-400 text-red-600 hover:bg-red-600 hover:text-white`
  }

  if (variant === 'error') {
    return `${className} bg-red-500 border-red-500 text-white hover:bg-red-700`
  }

  return `${className} border-black text-black hover:bg-black hover:text-white`
}

const Button = ({
  variant,
  onClick,
  isLoading,
  children,
}: ButtonProps) => (
  <div>
    <div className={isLoading ? 'absolute mix-blend-multiply bg-gray-200 rounded w-24 h-12' : ''} />
    <div
      className={getClassByVariant(variant)}
      onClick={isLoading ? undefined : onClick}
    >
      {children}
    </div>
  </div>
)


Button.propTypes = {
  variant: PropTypes.oneOf(['default', 'primary', 'danger', 'error']),
  isLoading: PropTypes.bool,
  children: PropTypes.node,
}

Button.defaultProps = {
  variant: 'default',
  onChange: () => {},
  isLoading: false,
  children: undefined,
}

Button.displayName = 'Button'


export default Button
