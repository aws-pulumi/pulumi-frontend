# FrontEnd app on AWS w/ Pulumi
Scripts to create and deploy React App on AWS infra with S3 Bucket hosting the static files, CloudFront Distribution as CDN, and optional custom domain name (like serving to `dev.my-app.com`) - all using [pulumi api](https://www.pulumi.com).

## [Target infra diagram](https://lucid.app/lucidchart/0dd69bdd-95cc-4d1e-95cb-f8538699af06/view)
## [![Diagram](https://gitlab.com/aws-pulumi/pulumi-frontend/-/raw/main/image.png)](https://lucid.app/lucidchart/0dd69bdd-95cc-4d1e-95cb-f8538699af06/view)

## Target infra components
After a [successful deploy](https://gitlab.com/aws-pulumi/pulumi-frontend/-/jobs/1467718728#L149), you'll have this infrastructure:
| AWS component | Description |
| :-- | :-- |
| S3 Bucket | Once our App gets build, all of its static files (HTML, CSS, JS, images) will be deployed to a Bucket in AWS |
| CloudFront Distribution | Makes our static files from S3 available all over the world using AWS Edge Locations. Could be tailored based on price/needs balance |
| Certificate Manager | Should you use a custom domain name then we need to create a SSL certificate for our HTTPS traffic. |
| Route53 Hosted Zone | Should you use a custom domain name then make sure your domain is available in the AWS Hosted Zone. In any case, Route53 will be used as DNS resolution for our app. |


## Tech stack
- [pulumi api](https://www.pulumi.com)
- [TypeScript](https://www.typescriptlang.org)
- React with [CRA](https://create-react-app.dev)
- State management with [React-Query](https://react-query.tanstack.com) + [Zustand](https://github.com/pmndrs/zustand)
- Styling [TailwindCSS](https://tailwindcss.com)


## Run locally
Make sure you have [pulumi installed](https://www.pulumi.com/docs/get-started/aws/begin/#install-pulumi).
```
git clone git@gitlab.com:aws-pulumi/pulumi-frontend.git
cd ./pulumi-frontend
yarn
cp .env.example .env
yarn run build
cd ./infra
pulumi up
```

Pulumi uses your AWS account to create the desired infrastructure
so once the scrip is completed
you can verify the result in your [AWS console](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances:)
or use your custom domain name, e.g. `dev.my-app.com`


## ENV VARs
You can change any of the default ENV VARs from `.env` file.
```
# Where is the BackEnd API server
REACT_APP_API_SERVER=https://swapi.dev/api

# After this many milliseconds, react-query will consider an API query as "stale".
# "Stale" queries are refetch on component re-render.
REACT_APP_QUERY_STATE_TIME=300000

# This variable is optional.
# If you'd like to deploy the app to a specific custom domain
# then make sure this `ALIAS_DOMAIN_NAME` is in AWS Hosted Zone
# ALIAS_DOMAIN_NAME=dev.my-app.com

# This variable is optional.
# If you set ENV VAR `ALIAS_DOMAIN_NAME` then set `ROUTE_53_HOSTED_ZONE_ID` as well.
# The `ID` prop of the Hosted Zone that has our `ALIAS_DOMAIN_NAME`
# ROUTE_53_HOSTED_ZONE_ID=00000000000000000000000

# This variable is optional.
# If you set ENV VAR `ALIAS_DOMAIN_NAME` then set `DOMAIN_NAME_ACM_CERTIFICATE_ARN` as well.
# When we serve HTTPS traffic to our `ALIAS_DOMAIN_NAME`
# we need to have a SSL Certificated from AWS ACM
# DOMAIN_NAME_ACM_CERTIFICATE_ARN=arn:aws:acm:eu-central-1:000000000000:certificate/00000000-00000-00000-00000-0000000000

# Which stack environment pulumi must use
PULUMI_STACK=dev
```

Deployment specific vars are set in `./infra/Pulumi.*.yaml`. They are rarely updated and well commented.
